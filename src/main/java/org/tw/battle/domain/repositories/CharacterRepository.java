package org.tw.battle.domain.repositories;
import	java.sql.SQLException;
import	java.util.ArrayList;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.tw.battle.infrastructure.DatabaseConnectionProvider.createConnection;

/**
 * @author Liu Xia
 */
public class CharacterRepository {
    private final ServiceConfiguration configuration;

    public CharacterRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int create(String name, int hp, int x, int y) throws Exception {
        // TODO:
        //   Please implement the method.
        //   Please refer to DatabaseConnectionProvider to see how to create connection.
        try (Connection connection = createConnection(configuration);
             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO character (name, hp, x, y) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)
        ) {
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, hp);
            preparedStatement.setInt(3, x);
            preparedStatement.setInt(4, y);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            return rs.getInt(1);
        }
    }
    public String info(int id) throws Exception {
        String sql = "SELECT * FROM character WHERE id = " + id;
        try (Connection connection = createConnection(configuration);
        ) {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next()) {
                String name = rs.getString(2);
                int hp = rs.getInt(3);
                int x = rs.getInt(4);
                int y = rs.getInt(5);
                String status = hp > 0? "alive" : "dead";
                return String.format("id: %d, name: %s, hp: %d, x: %d, y: %d, status: %s", id, name, hp, x, y, status);
            }
            return "Bad command: character not exist";
        }
    }
    public boolean rename(int id, String name) throws Exception {
        try (Connection connection = createConnection(configuration);
             PreparedStatement preparedStatement = connection.prepareStatement("UPDATE character SET `name` = ? WHERE `id` = ?")
        ) {
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return false;
    }
}
