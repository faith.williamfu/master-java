package org.tw.battle.domain;

/**
 * @author Liu Xia
 *
 * IMPORTANT: You cannot modify this file.
 */
@SuppressWarnings("unused")
public class CommandResponse {
    private final boolean success;
    private final String message;
    private final boolean quit;

    private CommandResponse(boolean success, String message, boolean quit) {
        this.success = success;
        this.message = message;
        this.quit = quit;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public boolean isQuit() {
        return quit;
    }

    public static CommandResponse success(String message) {
        return new CommandResponse(true, message, false);
    }

    public static CommandResponse fail(String message) {
        return new CommandResponse(false, message, false);
    }

    public static CommandResponse quit() {
        return new CommandResponse(true, null, true);
    }
}
