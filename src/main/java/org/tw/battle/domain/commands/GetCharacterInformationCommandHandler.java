package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

import java.util.Arrays;

/**
 * @author Liu Xia
 */
public class GetCharacterInformationCommandHandler implements CommandHandler {
    // TODO: Please implement the command handler.
    private static final String CMD_NAME = "character-info";
    private final CharacterRepository characterRepository;

    public GetCharacterInformationCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if (commandArgs.length == 0) {
            return CommandResponse.fail("Bad command: character-info <character id>");
        }
        if (commandArgs.length > 1) {
            return CommandResponse.fail("Bad command: character-info <character id>");
        }
        String message = characterRepository.info(Integer.parseInt(commandArgs[0]));
        if (!message.equals("Bad command: character not exist")) {
            return CommandResponse.success(message);
        }
        return CommandResponse.fail(message);
    }
}
