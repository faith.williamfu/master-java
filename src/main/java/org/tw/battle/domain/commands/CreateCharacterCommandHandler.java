package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

/**
 * @author Liu Xia
 */
public class CreateCharacterCommandHandler implements CommandHandler {
    private static final String CMD_NAME = "create-character";
    private final CharacterRepository characterRepository;

    public CreateCharacterCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if (commandArgs.length != 0) {
            return CommandResponse.fail(String.format("Bad command: %s", CMD_NAME));
        }

        final int id = characterRepository.create("unnamed", 100, 0, 0);
        String message = String.format("Character created: %d", id);
        return CommandResponse.success(message);
    }
}
