package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

import java.util.Arrays;

/**
 * @author Liu Xia
 */
public class RenameCharacterCommandHandler implements CommandHandler {
    // TODO: Please implement the command handler.
    private static final String CMD_NAME = "rename-character";
    private final CharacterRepository characterRepository;

    public RenameCharacterCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if (commandArgs.length != 2) {
            return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
        }
        if (commandArgs[1].length() < 3 || commandArgs[1].length() > 64) {
            return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
        }
        if (!commandArgs[0].matches("[0123456789]+")) {
            return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
        }
        if (characterRepository.info(Integer.parseInt(commandArgs[0])).equals("Bad command: character not exist")) {
            return CommandResponse.fail("Bad command: character not exist");
        }
        if (!commandArgs[1].matches("[a-zA-Z-]+")) {
            return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
        }
        if (characterRepository.rename(Integer.parseInt(commandArgs[0]), commandArgs[1])){
           String message = "Character renamed: id: " + commandArgs[0] + ", name: " + commandArgs[1];
           return CommandResponse.success(message);
        }
        return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
    }
}
