package org.tw.battle.mocks;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;

import java.util.Objects;

public class MockCommandHandler implements CommandHandler {
    private final String commandName;
    private final boolean success;
    private final String message;
    private String[] commandArgs;
    private boolean called;

    public MockCommandHandler(String commandName, boolean success, String message) {
        this.commandName = Objects.requireNonNull(commandName);
        this.success = success;
        this.message = message;
    }

    @Override
    public boolean canHandle(String commandName) {
        return this.commandName.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) {
        this.commandArgs = commandArgs;
        this.called = true;
        if (success) { return CommandResponse.success(message); }
        return CommandResponse.fail(message);
    }

    public String[] getCommandArgs() {
        return commandArgs;
    }

    public boolean isCalled() {
        return called;
    }
}
