package org.tw.battle;

import org.jooq.Record;
import org.junit.jupiter.api.Test;
import org.tw.battle.dbTest.InMemoryDbSupport;
import org.tw.battle.dbTest.TestQueryHelper;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.Game;

import static org.junit.jupiter.api.Assertions.*;
import static org.tw.battle.GameTestHelper.createGame;
import static org.tw.battle.GameTestHelper.getCharacterIdFromResponse;

@InMemoryDbSupport
class CreateCharacterTest {
    @Test
    void should_create_character() {
        final Game game = createGame();
        final CommandResponse response = game.execute("create-character");

        assertTrue(response.isSuccess());
        assertEquals("Character created: 1", response.getMessage());
        assertEquals(1, getCharacterIdFromResponse(response));
    }

    @Test
    void should_save_character_when_created() throws Exception {
        final Game game = createGame();
        final CommandResponse response = game.execute("create-character");

        assertTrue(response.isSuccess());
        assertEquals(1, TestQueryHelper.getCharacterNumber());
    }

    @Test
    void should_save_character_with_initial_value() throws Exception {
        final Game game = createGame();
        final CommandResponse response = game.execute("create-character");

        final Record character = TestQueryHelper.getCharacter(getCharacterIdFromResponse(response));

        assertEquals("unnamed", character.getValue("name", String.class));
        assertEquals(100, character.getValue("hp", Integer.class));
        assertEquals(0, character.getValue("x", Integer.class));
        assertEquals(0, character.getValue("y", Integer.class));
    }

    @Test
    void should_return_bad_command_with_usage_if_add_additional_argument() {
        final Game game = createGame();
        final CommandResponse response = game.execute("create-character", "additional");

        assertFalse(response.isSuccess());
        assertEquals("Bad command: create-character", response.getMessage());
    }

    @Test
    void should_not_create_character_if_we_enters_a_bad_command() throws Exception {
        final Game game = createGame();
        final CommandResponse response = game.execute("create-character", "additional");

        assertFalse(response.isSuccess());
        assertEquals(0, TestQueryHelper.getCharacterNumber());
    }
}
