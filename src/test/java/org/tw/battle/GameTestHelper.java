package org.tw.battle;

import org.tw.battle.dbTest.TestDatabaseConfiguration;
import org.tw.battle.domain.CommandHandlerProvider;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.Game;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameTestHelper {
    public static Game createGame() {
        return new Game(new CommandHandlerProvider().createHandlers(TestDatabaseConfiguration.getConfiguration()));
    }

    public static Integer createCharacter(Game game) {
        final CommandResponse response = game.execute("create-character");
        assertTrue(response.isSuccess());
        return getCharacterIdFromResponse(response);
    }

    public static Integer getCharacterIdFromResponse(CommandResponse response) {
        final Pattern pattern = Pattern.compile("^Character created: (?<id>[0-9]+)$");
        final String message = response.getMessage();
        final Matcher matcher = pattern.matcher(message);
        assertTrue(matcher.matches(), message);
        return Integer.parseInt(matcher.group("id"));
    }
}
