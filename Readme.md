# Final Evaluation (Backend Story)

This material should be used in conjunction with the story reference.

## IMPORTANT

You cannot change all the code in the project. Please refer to the next diagram:

```
src
 |- main
 |   |- java <--- You can modify source code and add additional file
 |   |            in this module. But you CANNOT modify following 
 |   |            files:
 |   |
 |   |            org.tw.battle.domain/CommandHandler.java
 |   |            org.tw.battle.domain/CommandResponse.java
 |   |            org.tw.battle.domain/Game.java
 |   |            org.tw.battle.domain/ServiceConfiguration.java
 |   |            org.tw.battle/GameFacade.java
 |   |            org.tw.battle/Program.java
 |   |
 |   |- resources <--- You can modify source code in this directory
 |                     but you CANNOT add new files.
 |
 |- test <--- You CANNOT change any file in this module
 |
 |- build.gradle  <--- You cannot modify this file
```

## Task 1: Create table

Now that you open the project, you can find that almost all the tests are failed. This is because there is no table in the database. So here are your tasks:

* Please find the file *resources\db\migration\V001__create_character_table.sql* in the project.
* This file will be executed everytime you start your applcation or running a test (please refer to *Production Executing Process* and *Test Executing Process* for more information). You should write SQL statement here to create a table called `character`.
* Please read the *Story 1 – Create Character* to get the column definitions of the table.

Once you finish the task. All tests in `DatabaseMigrationTest` will pass.

## Task 2: Implement Story 1

In this story, you will implement creating character function. Here are the tasks:

* Please read *Story 1 – Create Character* carefully to get the requirement.
* Please read the source code to get the idea how a command is executed.
* Please implement the method `int create(String name, int hp, int x, int y)` in `CharacterRepository`.

Once you finish the task. ALl tests `CreateCharacterTest` will pass.

## Task 3: Implement Story 2

Please implement `GetCharacterInformationCommandHandler` according to the acceptance criteria of *Story 2 – Get Character Information*. Here are some hints:

* Please read *Story 2 – Get Character Information* carefully to get the requirement.
* You can find the source code at *src/main/java/org/tw/battle/domain/commands/GetCharacterInformationCommandHandler.java*. The file has been created in advance.
* You may also have to modify other source code (including add new class, add new method, etc.) to help you completing the requirement.
* If you don't know how to start. You can refer to the implementation of `CreateCharacterCommandHandler`. You can read existing source code to get the idea how a command is executed from `Game` class to certain command handlers.

If the implementation is correct then all the test in `GetCharacterInformationTest` will pass.

## Task 4: Implement Story 3

I am pretty sure that you have got the idea of the program. So in this task, we will not create source code files for you. You have to choose the right place to create the source code file and implement all the functions.

* Please read *Story 3 – Renaming Character* carefully to get the requirement.
* You have to create source code file by yourself. I suggest that you review the structure of the program and choose the right package to create your code file. You may also have to modify other source code to help you completing the requirement.

If the implement is correct, then all the tests will pass.

###test